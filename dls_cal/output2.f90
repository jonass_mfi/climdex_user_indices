! subroutine used by dls_1.1.f90
!
! This subroutine is used to output dls values (after interpolation) in ASCII format.
! also contains the lat and lon dimensions
! and grid file name
! to be used by gridding project.
!
! input:
!      none
!   (transfer data through the module COMM)
!

subroutine output
 use COMM

  print*,''
  print*,'finish calculating, now output results ONLY in ascii file ...'
 call out_txt

return
end subroutine output
