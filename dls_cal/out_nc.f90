! subroutine used by dls_1.1.f90
!
! used to output dls values after interpolation in ASCII and NETCDF formats.
! also contains the lat and lon dimensions
! and grid file name
! to be used by gridding project.
!
! input:
!      none
!   (transfer data through the module COMM)
!

subroutine output
 use COMM

  print*,''
  print*,'finish calculating, now output results in ascii and netcdf files ..'
 call out_txt    ! output ASCII file
 call out_nc     ! output netcdf file

return
end subroutine output


! output netcdf file
! use Fortran 90 interface of netcdf library.
!
 subroutine out_nc
    use COMM
    use netcdf    ! This is for fortran90 interface only
    implicit none

!... define the variables used in this program !
    character*250 :: Out_file
    integer :: ncid, LATID, LONID,i,n              ! dimention's ID
    integer varID_lon,varID_lat,ID_f(13)           ! variable ID.
    integer dataDim(2)

    Out_file=trim(outfile)//'.nc'

!... creat a new NetCDF file
    if(NF90_create(Out_file, NF90_clobber, ncid) /=NF90_noerr) then
      print*,'Error: can NOT create the following output file:'
      print*,trim(Out_file)
      stop 'Now the code stops ...'
    endif

!... define dimentions
    call err_handle(NF90_def_dim(ncid, 'lon', Nlon, LONID),'define "lon" dimension')
    call err_handle(NF90_def_dim(ncid, 'lat', Nlat, LATID),'define "lat" dimension')

    call err_handle(NF90_def_var(ncid, 'lon', NF90_float, lonID,varID_lon),'define var "lon"')
    call err_handle(NF90_def_var(ncid, 'lat', NF90_float, latID,varID_lat),'define var "lat"')

!... define varibles
    datadim=(/lonid,latid/)

    do i=1,13-monthf+1
      call err_handle(NF90_def_var(ncid, monthnames(monthf+i-2), NF90_float, datadim, ID_f(i)),'define data var')
      call err_handle(NF90_put_att(ncid, ID_f(i), 'missing_value',mdi),'put att for data var')
      call err_handle(NF90_put_att(ncid, ID_f(i), '_FillValue', mdi),'put att for data var')
    enddo

!... Put attribute of variables
    call err_handle(NF90_put_att(ncid, NF90_global, 'Title', 'dls '),'define global title')
    call err_handle(NF90_put_att(ncid, NF90_global, 'author',  'Hongang Yang - hongang.yang@unsw.edu.au ') ,'define global author')
    call err_handle(NF90_put_att(ncid, NF90_global, 'history', 'Created from dls version 1.1 '),'define global history')
    call err_handle(NF90_put_att(ncid, NF90_global, 'units',  'km '),'define global unit')
    call err_handle(NF90_put_att(ncid, NF90_global, 'long_name', 'Decorrelation Length Scale '),'define global long_name')
    call err_handle(NF90_put_att(ncid, NF90_global, 'mskfile', trim(grid_file)),'define global maskfile name')

    call err_handle(NF90_put_att(ncid, varID_lon, 'long_name', 'Longitude'),'put att for "lon"')
    call err_handle(NF90_put_att(ncid, varID_lon, 'units', 'degrees_east'),'put att for "lon"')

    call err_handle(NF90_put_att(ncid, varID_lat, 'long_name', 'Latitude'),'put att for "lat"')
    call err_handle(NF90_put_att(ncid, varID_lat, 'units',  'degrees_north'),'put att for "lat"')

!... end of define mode
    call err_handle(NF90_enddef(ncid),'end define')

!... To save dimension data !
    call err_handle(NF90_put_var(ncid, varID_lon, lon),'save "lon" var')
    call err_handle(NF90_put_var(ncid, varID_lat, lat),'save "lat" var')

!... To put data in to this file!
    do i=1,13-monthf+1
      call err_handle(NF90_put_var(ncid, ID_f(i), dlss(:,:,i)),'save dls var')
    enddo

!... close this file..
    call err_handle(NF90_close(ncid),'close nc file')

    return
    end subroutine out_nc


! This subroutine can be used to
! check whether there's error in creating/writing/closing the netcdf file.
! It also gives error message for further investigation.
! input:
!      status: integer, to be checked with NF90_noerr
!      string: character, containing error message
! output:
!      none

subroutine err_handle(status,string)
 use netcdf
 implicit none
 integer       :: status
 character*(*) :: string

 if(status/=NF90_noerr) then
   print*,'Error in Netcdf function: '//trim(string)
   print*,'Error code: ',status
   print*,'Please check the "out_nc" subroutine.'
   stop 'now code stops ...'
 endif

 return
 end subroutine err_handle
