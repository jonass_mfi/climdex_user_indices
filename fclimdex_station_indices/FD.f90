! ---------- play all ETCCDI indices subroutines ----------
    subroutine exec_all_daily_temperature
    use COMM
    use functions

    character(40), parameter :: name_su='SU', name_id='ID', name_tr='TR'

    call FD
    call SU(name_su, 25)
    call IDT(name_id, 0)
    call TR(name_tr, 20)

    return 
    end subroutine exec_all_daily_temperature

! ---------- Subroutine ID ----------
    subroutine IDT(name, TX)
    use COMM
    use functions

    implicit none
    character(40), parameter  :: indice_type='ID'
    character(40), intent(in) :: name
    real, intent(in) :: TX
    integer      :: year, trno, kth, month,i,j,day, Ky
    real         :: oout(YRS)

    if(Tmax_miss .and. Tmin_miss) return

    trno=0
    oout=0
    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      do month=1,12
        Kth=Mon(month,Ky)
        do day=1,kth
          trno=trno+1
          if(YMD(trno,3).ne.day) then
            print *, 'ERROR1 at FD!!!'
            stop
          endif
          if(nomiss(TMAX(trno)).and.TMAX(trno).lt.TX)  &
             oout(i)=oout(i)+1
        enddo
      enddo
    enddo

    do i=1,YRS
      if(YNASTAT(i,2)==1) then
              oout(i)=MISSING  ! ID
      endif
    enddo

    if(.not. Tmax_miss) then
      call save_annual_folder(indice_type, name, oout)
    endif

    return
    end subroutine IDT

! ---------- Subroutine SU ----------
    subroutine SU(name, TX)
    use COMM
    use functions

    implicit none
    character(40), parameter  :: indice_type='SU'
    character(40), intent(in) :: name
    real, intent(in) :: TX
    integer      :: year, trno, kth, month,i,j,day, Ky
    real         :: oout(YRS)

    if(Tmax_miss .and. Tmin_miss) return

      ! loop for all days (Year, Month, Day)
      trno=0
      oout=0
      do i=1,YRS
        year=i+SYEAR-1
        Ky=leapyear(year)+1
        do month=1,12
          Kth=Mon(month,Ky)
          do day=1,kth
            trno=trno+1
            if(YMD(trno,3).ne.day) then
              print *, 'ERROR1 at FD!!!'
              stop
            endif
            if(nomiss(TMAX(trno)).and.TMAX(trno).gt.TX) &
               oout(i)=oout(i)+1
          enddo
        enddo
      enddo

      ! check if annual TX, TN is MISSING
      ! if yes, set index=MISSING
      do i=1,YRS
        if(YNASTAT(i,2)==1) then
                oout(i)=MISSING  ! SU
        endif
      enddo

      ! output the index
      if(.not. Tmax_miss) then
        call save_annual_folder(indice_type, name, oout)
      endif

    return
    end subroutine SU

! ---------- Subroutine TR ----------

    subroutine TR(name, TN)
    use COMM
    use functions

    implicit none
    character(40), parameter  :: indice_type='TR'
    character(40), intent(in) :: name
    real, intent(in) :: TN
    integer      :: year, trno, kth, month,i,j,day, Ky
    real         :: oout(YRS)

    if(Tmax_miss .and. Tmin_miss) return

    trno=0
    oout=0
    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      do month=1,12
        Kth=Mon(month,Ky)
        do day=1,kth
          trno=trno+1
          if(YMD(trno,3).ne.day) then
            print *, 'ERROR1 at FD!!!'
            stop
          endif
          if(nomiss(TMIN(trno)).and.TMIN(trno).gt.TN) &
             oout(i)=oout(i)+1
        enddo
      enddo
    enddo

    do i=1,YRS
      if(YNASTAT(i,3)==1) then
              oout(i)=MISSING  ! TR
      endif
    enddo

    if(.not. Tmin_miss) then
      call save_annual_folder(indice_type, name, oout)
    endif

    return
    end subroutine TR

! subroutines used by fclimdex_station, to calculate indices: FD,SU,ID,TR.
!
! definitions:
! FD, Number of frost days: Annual count of days when TN (daily minimum temperature) < 0 degree.
! SU, Number of summer days: Annual count of days when TX (daily maximum temperature) > 25 degree.
! ID, Number of icing days: Annual count of days when TX (daily maximum temperature) < 0 degree.
! TR, Number of tropical nights: Annual count of days when TN (daily minimum temperature) > 20 degree.
!
! input
!    none
!   (transfer data through the module COMM)
!
    subroutine FD
    use COMM
    use functions

    implicit none
    character(40), parameter  :: indice_type='FD'
    character(200), parameter :: name='FD'
    integer      :: year, trno, kth, month,i,j,day, Ky
    real         :: oout(YRS)   

    if(Tmax_miss .and. Tmin_miss) return

    ! loop for all days (Year, Month, Day)
    trno=0
    oout=0
    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      do month=1,12
        Kth=Mon(month,Ky)
        do day=1,kth
          trno=trno+1
          if(YMD(trno,3).ne.day) then
            print *, 'ERROR1 at FD!!!'
            stop
          endif
          if(nomiss(TMIN(trno)).and.TMIN(trno).lt.0) 	&
             oout(i)=oout(i)+1
        enddo
      enddo
    enddo

    ! check if annual TX, TN is MISSING
    ! if yes, set index=MISSING
    do i=1,YRS
      if(YNASTAT(i,3)==1) then
              oout(i)=MISSING  ! FD
      endif
    enddo

    ! output the index

    if(.not. Tmin_miss) then
      call save_annual_folder(indice_type, name, oout)
    endif

    return
    end subroutine FD
