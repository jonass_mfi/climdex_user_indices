! -----------------------------------------------------------------
! subroutines used by fclimdex_station, to calculate index: CDD.
!
! definitions:
! CDD: Maximum length of dry spell, maximum number of consecutive days with RR < 1mm.
!
! input
!    none
!   (transfer data through the module COMM)

    subroutine CDD(name, min_rr)
    use COMM
    use functions

    implicit none
    character(40), parameter  :: indice_type='CDD'
    character(40), intent(in) :: name
    integer, intent(in)       :: min_rr
    integer                   :: year, month, day, kth, cnt,i, Ky   ! loop index and/or count #
    real                      :: oout(YRS), &        ! annual index to be calculated
                                 nncdd               ! count #

    if(Prcp_miss) return   ! if all PRCP missing, ignore this step

    ! get annual index
    cnt=0
    oout=0.
    do i=1,YRS
      if(i==1)nncdd=0.
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      do month=1,12
           kth=mon(month,Ky)
        do day=1,kth
          cnt=cnt+1
          if(ismiss(PRCP(cnt))) then  ! CDD stops, reset the # to zero !
            if(nncdd.gt.oout(i)) oout(i)=nncdd ! from Markus
            nncdd=0.
          elseif(PRCP(cnt).lt.min_rr) then  ! CDD continues, CWD stops
            nncdd=nncdd+1.
          else                         ! CWD continues, CDD stops
            if(nncdd.gt.oout(i)) oout(i)=nncdd
            nncdd=0.
          endif
        enddo  ! loop for day
      enddo    ! loop for month


      if(oout(i).lt.nncdd) then
        if(year.eq.EYEAR) then        ! Is it the end year?
                oout(i)=nncdd
        elseif(PRCP(cnt+1).ge.min_rr.or.ismiss(PRCP(cnt+1)))then  ! not end year, but next day is wet.
                oout(i)=nncdd
                nncdd=0.
        endif
        if(oout(i).eq.0) then
          write(ID_log,'(a20,1x,i4,a)') stnID,year,': CDD=0 !'
          oout(i)=MISSING  ! Why only set oout? answer: Should be no no-dry days, but should put error log file!
        endif
      endif

      ! check if annual PRCP is MISSING
      ! if yes, set index=MISSING
      if(YNASTAT(i,1).eq.1) then
        oout(i)=MISSING
      endif

    enddo     ! loop for year

    ! output the index
    call save_annual_folder(indice_type, name, oout)

    return
    end subroutine CDD

! -----------------------------------------------------------------
! subroutines used by fclimdex_station, to calculate index: CWD.
!
! definitions:
! CWD: Maximum length of wet spell, maximum number of consecutive days with RR ≥ 1mm
!
! input
!    none
!   (transfer data through the module COMM)

    subroutine CWD(name, min_rr)
    use COMM
    use functions

    implicit none
    character(40), parameter  :: indice_type='CWD'
    character(40), intent(in) :: name
    integer, intent(in)       :: min_rr
    integer                   :: year, month, day, kth, cnt, i, Ky   ! loop index and/or count #
    real                      :: oout(YRS), &     ! annual index to be calculated
                                 nncwd            ! count #

    if(Prcp_miss) return   ! if all PRCP missing, ignore this step

    ! get annual index
    cnt=0
    oout=0.
    do i=1,YRS
      if(i==1)nncwd=0.
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      do month=1,12
           kth=mon(month,Ky)
        do day=1,kth
          cnt=cnt+1
          if(ismiss(PRCP(cnt))) then  ! CDD and CWD stops, reset the # to zero !
            if(nncwd.gt.oout(i)) oout(i)=nncwd ! from Markus
            nncwd=0.
          elseif(PRCP(cnt).lt.min_rr) then  ! CDD continues, CWD stops
            if(nncwd.gt.oout(i)) oout(i)=nncwd
            nncwd=0.
          else                         ! CWD continues, CDD stops
            nncwd=nncwd+1.
          endif
        enddo  ! loop for day
      enddo    ! loop for month

      if(oout(i).lt.nncwd) then
        if(year.eq.EYEAR) then         ! Is it the end year?
                oout(i)=nncwd
        elseif(PRCP(cnt+1).lt.min_rr.or.ismiss(PRCP(cnt+1)))then  ! not end year, but next day is dry.
                oout(i)=nncwd
                nncwd=0.
        endif
      endif

      ! check if annual PRCP is MISSING
      ! if yes, set index=MISSING
      if(YNASTAT(i,1).eq.1) then
        oout(i)=MISSING
      endif

    enddo     ! loop for year

    ! output the index
    call save_annual_folder(indice_type, name, oout)

    return
    end subroutine CWD
    
! ---------- play all ETCCDI indices subroutines ----------
    subroutine exec_all_precipitation_spell
    use COMM
    use functions

    character(40), parameter :: name_CDD='CDD', name_CWD='CWD'

    call CDD(name_CDD, 1)
    call CWD(name_CWD, 1)

    return
    end subroutine exec_all_precipitation_spell
