! -------------------------------------------------------------------
! Calculate index TN10p
!  
! definitions are:
! TN10p: Percentage of days when TN < 10th percentile.
! To avoid possible inhomogeneity across the in-base and out-base periods, 
!    the calculation for the base period requires the use of a bootstrap processure. 
!    Details are described in Zhang et al. (2004).
!
! NOTE that the index allows at most 10 MISSING days in a month !!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! input
!    none
!   (transfer data through the module COMM)
    subroutine TN10p(name, TN10pLevel)
    use COMM
    use functions

    implicit none
    character(40), parameter        :: indice_type='TN10p'
    character(40), intent(in)       :: name
    character(40)                   :: threshold_name
    real, intent(in)                :: TN10pLevel
    integer:: year, month, day, kth, cnt, nn, missncnt, & 
              iter, cntn,i,byear,flgtn,flg, Ky     ! loop index, count #, error indicator

    real,dimension(DoY,BYRS,BYRS-1) :: thresbn10
    real,dimension(DoY)             :: threstmp          ! temporary threshold data
    real,dimension(BYRS,DoY+2*SS)   :: tndata, tnboot    ! extended TX and TN data in base period
    real,dimension(DoY)             :: thresan10         ! thresholds
    real,dimension(BYRS,DoY)        :: tndtmp            ! base year TN.
    real,dimension(YRS,13)          :: tn10out
    real :: BYRSm,rDoY,temp

    integer :: SmByear ! SYear-BaseYear

    if(Tmax_miss .and. Tmin_miss) return

    BYRSm=BYRS-1.0
    rDoY=100.0/DoY
    cnt=0
    nn=0
    tndtmp=MISSING

    ! choose TX and TN in base years, excluding 29th of Feb.
    SmByear=SYEAR-BASESYEAR
    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      nn=0
      do month=1,12
        Kth=Mon(month,Ky)
        do day=1,kth
          cnt=cnt+1
          if(year.ge.BASESYEAR.and.year.le.BASEEYEAR.and.(month.ne.2 &
            .or.day.ne.29))then
            nn=nn+1
            tndtmp(i+SmByear,nn)=TMIN(cnt)
          endif
        enddo
      enddo
      if(year.ge.BASESYEAR.and.year.le.BASEEYEAR.and.nn.ne.DoY)then
        print *,"date count error in TX10p!", nn
        stop
      endif
    enddo

    ! extended TN in base years
    tndata(1,1:SS)=tndtmp(1,1)  ! i=1
    tndata(2:BYRS,1:SS)=tndtmp(1:BYRS-1,DoY+1-SS:DoY) ! i /=1
    tndata(:,1+SS:DoY+SS)=tndtmp(:,1:DoY)
    tndata(BYRS,1+DoY+SS:DoY+SS*2)=tndtmp(BYRS,DoY)
    tndata(1:BYRS-1,1+DoY+SS:DoY+SS*2)=tndtmp(2:BYRS,1:SS)

    flgtn=0
    call single_threshold(tndata,TN10pLevel,threstmp,flgtn)  ! thresholds for TN
      thresan10(:)=threstmp(:)-1e-5
    if(flgtn.eq.1) then
      write(6,*) "TMIN Missing value overflow in exceedance rate"
      tn10out=MISSING
    endif

    if(Tmin_miss) flgtn=1

    !$OMP parallel do default(shared) private(i,nn,txboot,tnboot,iter,threstmp,flg)
    do i=1,BYRS   ! This part consumes most of the time, due to "threshold"...
      tnboot=tndata
      nn=0
      do iter=1,BYRS
        if(iter.ne.i) then
          nn=nn+1
            if(flgtn.eq.0) tnboot(i,:)=tnboot(iter,:)

          if(flgtn.eq.0) then
            call single_threshold(tnboot,TN10pLevel,threstmp,flgtn)
              thresbn10(:,i,nn)=threstmp(:)-1e-5
          endif
        endif
      enddo
    enddo

    if(flgtn.eq.0)then
      tn10out=0.
    endif

    cnt=0
    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      byear=year-BASESYEAR+1
      nn=0
      do month=1,12
        missncnt=0
        Kth=Mon(month,Ky)
        do day=1,kth
          if(month.ne.2.or.day.ne.29) nn=nn+1
          cnt=cnt+1
          if(nomiss(TMIN(cnt)))then       ! for TN
            if(year.lt.BASESYEAR.or.year.gt.BASEEYEAR) then   ! outside the base years
              if(TMIN(cnt).lt.thresan10(nn)) tn10out(i,month)= tn10out(i,month)+1
            else
              do iter=1,BYRS-1         ! inside the base years
                if(TMIN(cnt).lt.thresbn10(nn,byear,iter))  tn10out(i,month)=tn10out(i,month)+1
              enddo
            endif
          else
            missncnt=missncnt+1
          endif
        enddo ! do day=1,kth

        if(year.ge.BASESYEAR.and.year.le.BASEEYEAR)then  ! inside base years
          tn10out(i,month)=tn10out(i,month)/BYRSm
        endif

        ! NOTE that the index allows at most 10 MISSING days in a month !!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if(missncnt.le.10.and.flgtn.eq.0)then   !  TN index
          temp=100./(kth-missncnt)
          tn10out(i,13)=tn10out(i,13)+tn10out(i,month)
          tn10out(i,month)=tn10out(i,month)*temp
        else
          tn10out(i,month)=MISSING
        endif
      enddo ! do month=1,12

      ! get annual index
      ! If annual TX/N is MISSING, set annual index=MISSING
      if(YNASTAT(i,3).eq.1.or.flgtn.eq.1) then
        tn10out(i,13)=MISSING
      endif
    enddo   ! year loop

130   continue

    ! output monthly index and/or thresholds.
    if(flgtn.eq.0)then
      call save_monthly_indice(indice_type, name, tn10out)
    endif

    ! to output thresholds as well, requested by Lisa @ 2011.08.01 (Jana Sillmann wants them)
    if(save_thresholds) then
      write(threshold_name, '(a2,i2,a)') "an", TN10pLevel*100, "p"
      call save_thres_indice(indice_type, name, threshold_name , thresan10)
    endif

    return

    end subroutine TN10p


! -------------------------------------------------------------------
! Calculate index TX10p
!  
! definitions are:
! TX10p: Percentage of days when TX < 10th percentile.
! To avoid possible inhomogeneity across the in-base and out-base periods, 
!    the calculation for the base period requires the use of a bootstrap processure. 
!    Details are described in Zhang et al. (2004).
!
! NOTE that the index allows at most 10 MISSING days in a month !!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! input
!    none
!   (transfer data through the module COMM)

    subroutine TX10p(name, TX10pLevel)
    use COMM
    use functions

    implicit none
    character(40), parameter        :: indice_type='TX10p'
    character(40), intent(in)       :: name
    character(40)                   :: threshold_name
    real, intent(in)                :: TX10pLevel
    integer:: year, month, day, kth, cnt, nn,  missxcnt, & 
              iter, cntx, i,byear,flgtx,flg, Ky          ! loop index, count #, error indicator

    real,dimension(DoY,BYRS,BYRS-1) :: thresbx10
    real,dimension(DoY)             :: threstmp          ! temporary threshold data
    real,dimension(BYRS,DoY+2*SS)   :: txdata, txboot    ! extended TX and TN data in base period
    real,dimension(DoY)             :: thresax10         ! thresholds
    real,dimension(BYRS,DoY)        :: txdtmp            ! base year TX
    real,dimension(YRS,13)          :: tx10out
    real :: BYRSm,rDoY,temp

    integer :: SmByear ! SYear-BaseYear

    if(Tmax_miss .and. Tmin_miss) return

    BYRSm=BYRS-1.0
    rDoY=100.0/DoY
    cnt=0
    nn=0
    txdtmp=MISSING

    ! choose TX and TN in base years, excluding 29th of Feb.
    SmByear=SYEAR-BASESYEAR
    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      nn=0
      do month=1,12
        Kth=Mon(month,Ky)
        do day=1,kth
          cnt=cnt+1
          if(year.ge.BASESYEAR.and.year.le.BASEEYEAR.and.(month.ne.2 &
            .or.day.ne.29))then
            nn=nn+1
            txdtmp(i+SmByear,nn)=TMAX(cnt)
          endif
        enddo
      enddo
      if(year.ge.BASESYEAR.and.year.le.BASEEYEAR.and.nn.ne.DoY)then
        print *,"date count error in TX10p!", nn
        stop
      endif
    enddo

    ! extended TN and TX in base years
    txdata(1,1:SS)=txdtmp(1,1)
    txdata(2:BYRS,1:SS)=txdtmp(1:BYRS-1,DoY+1-SS:DoY)
    ! out-bounds found by Imke, corrected on 2012.7.18, H.Yang @ CCRC.
    txdata(:,1+SS:DoY+SS)=txdtmp(:,1:DoY)
    txdata(BYRS,1+DoY+SS:DoY+SS*2)=txdtmp(BYRS,DoY)
    txdata(1:BYRS-1,1+DoY+SS:DoY+SS*2)=txdtmp(2:BYRS,1:SS)

    flgtx=0
    call single_threshold(txdata,TX10pLevel,threstmp,flgtx)  ! thresholds for TX
      thresax10(:)=threstmp(:)-1e-5
    if(flgtx.eq.1) then
      write(6,*) "TMAX Missing value overflow in exceedance rate"
      tx10out=MISSING
    endif

    if(Tmax_miss) flgtx=1  ! if all missing, don't output

    do i=1,BYRS   ! This part consumes most of the time, due to "threshold"...
      txboot=txdata
      nn=0
      do iter=1,BYRS
        if(iter.ne.i) then
          nn=nn+1
            if(flgtx.eq.0) txboot(i,:)=txboot(iter,:)
            
          if(flgtx.eq.0)then
            call single_threshold(txboot,TX10pLevel,threstmp,flgtx)
              thresbx10(:,i,nn)=threstmp(:)-1e-5
          endif
        endif
      enddo
    enddo

    if(flgtx.eq.0)then
      tx10out=0.
    endif

    cnt=0
    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      byear=year-BASESYEAR+1
      nn=0
      do month=1,12
        missxcnt=0
        Kth=Mon(month,Ky)
        do day=1,kth
          if(month.ne.2.or.day.ne.29) nn=nn+1
          cnt=cnt+1
          if(nomiss(TMAX(cnt)))then       ! for TX
            if(year.lt.BASESYEAR.or.year.gt.BASEEYEAR) then  ! outside the base years
              if(TMAX(cnt).lt.thresax10(nn)) tx10out(i,month)= tx10out(i,month)+1
            else
              do iter=1,BYRS-1       ! inside base years
                if(TMAX(cnt).lt.thresbx10(nn,byear,iter))   tx10out(i,month)=tx10out(i,month)+1
              enddo
            endif
          else
            missxcnt=missxcnt+1
          endif
        enddo ! do day=1,kth

        if(year.ge.BASESYEAR.and.year.le.BASEEYEAR)then  ! inside base years
          tx10out(i,month)=tx10out(i,month)/BYRSm
        endif


! NOTE that the index allows at most 10 MISSING days in a month !!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if(missxcnt.le.10.and.flgtx.eq.0)then   ! TX index
          temp=100./(kth-missxcnt)
          tx10out(i,13)=tx10out(i,13)+tx10out(i,month)
          tx10out(i,month)=tx10out(i,month)*temp
        else
          tx10out(i,month)=MISSING
        endif
      enddo ! do month=1,12

! get annual index
! If annual TX/N is MISSING, set annual index=MISSING
      if(YNASTAT(i,2).eq.1.or.flgtx.eq.1) then
        tx10out(i,13)=MISSING
      else
        tx10out(i,13)=tx10out(i,13)*rDoY
      endif
    enddo   ! year loop

130   continue

    ! output monthly index and/or thresholds.
    if(flgtx.eq.0)then
      call save_monthly_indice(indice_type, name, tx10out)
    endif

    ! to output thresholds as well, requested by Lisa @ 2011.08.01 (Jana Sillmann wants them)
    if(save_thresholds) then
      write(threshold_name, '(a2,i2,a)') "ax", TX10pLevel*100, "p"
      call save_thres_indice(indice_type, name, threshold_name, thresax10)
    endif

    return
    end subroutine TX10p

! -------------------------------------------------------------------
! Calculate index TN90p
!  
! definitions are:
! TN90p: Percentage of days when TN > 90th percentile
! To avoid possible inhomogeneity across the in-base and out-base periods, 
!    the calculation for the base period requires the use of a bootstrap processure. 
!    Details are described in Zhang et al. (2004).
!
! NOTE that the index allows at most 10 MISSING days in a month !!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! input
!    none
!   (transfer data through the module COMM)

    subroutine TN90p(indice_type, name, TN90pLevel)
    use COMM
    use functions

    implicit none
    character(40), intent(in)       :: indice_type, name
    character(40)                   :: threshold_name
    real, intent(in)                :: TN90pLevel
    integer:: year, month, day, kth, cnt, nn,  missncnt, & 
              iter, cntx, cntn,i,byear,flgtn,flg, Ky     ! loop index, count #, error indicator

    real,dimension(DoY,BYRS,BYRS-1) :: thresbn90
    real,dimension(DoY)             :: threstmp          ! temporary threshold data
    real,dimension(BYRS,DoY+2*SS)   :: tndata, &  ! extended TX and TN data in base period
                                       tnboot
    real,dimension(DoY)             :: thresan90         ! thresholds
    real,dimension(BYRS,DoY)        :: txdtmp,tndtmp     ! base year TX and TN.
    real,dimension(YRS,13)          :: tn90out           ! monthly index
    real,dimension(YRS)             :: wsdi,csdi         ! annual index
    real :: BYRSm,rDoY,temp

    integer :: SmByear ! SYear-BaseYear

    if(Tmax_miss .and. Tmin_miss) return

    BYRSm=BYRS-1.0
    rDoY=100.0/DoY
    cnt=0
    nn=0
    tndtmp=MISSING

    ! choose TN in base years, excluding 29th of Feb.
    SmByear=SYEAR-BASESYEAR
    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      nn=0
      do month=1,12
        Kth=Mon(month,Ky)
        do day=1,kth
          cnt=cnt+1
          if(year.ge.BASESYEAR.and.year.le.BASEEYEAR.and.(month.ne.2 &
            .or.day.ne.29))then
            nn=nn+1
            tndtmp(i+SmByear,nn)=TMIN(cnt)
          endif
        enddo
      enddo
      if(year.ge.BASESYEAR.and.year.le.BASEEYEAR.and.nn.ne.DoY)then
        print *,"date count error in TX10p!", nn
        stop
      endif
    enddo

    ! extended TN and TX in base years
    tndata(1,1:SS)=tndtmp(1,1)  ! i=1
    tndata(2:BYRS,1:SS)=tndtmp(1:BYRS-1,DoY+1-SS:DoY) ! i /=1
    ! out-bounds found by Imke, corrected on 2012.7.18, H.Yang @ CCRC.
    tndata(:,1+SS:DoY+SS)=tndtmp(:,1:DoY)
    tndata(BYRS,1+DoY+SS:DoY+SS*2)=tndtmp(BYRS,DoY)
    tndata(1:BYRS-1,1+DoY+SS:DoY+SS*2)=tndtmp(2:BYRS,1:SS)

    flgtn=0
    call single_threshold(tndata,TN90pLevel,threstmp,flgtn)  ! thresholds for TN
      thresan90(:)=threstmp(:)+1e-5
    if(flgtn.eq.1) then
      write(6,*) "TMIN Missing value overflow in exceedance rate"
      tn90out=MISSING
    endif

    if(Tmin_miss) flgtn=1

    do i=1,BYRS   ! This part consumes most of the time, due to "threshold"...
      tnboot=tndata
      nn=0
      do iter=1,BYRS
        if(iter.ne.i) then
          nn=nn+1
          if(flgtn.eq.0) then
            tnboot(i,:)=tnboot(iter,:)
            call single_threshold(tnboot,TN90pLevel,threstmp,flgtn)
            thresbn90(:,i,nn)=threstmp(:)+1e-5
          endif
        endif
      enddo
    enddo


    if(flgtn.eq.0)then
      tn90out=0.
    endif

    cnt=0
    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      byear=year-BASESYEAR+1
      nn=0
      do month=1,12
        missncnt=0
        Kth=Mon(month,Ky)
        do day=1,kth
          if(month.ne.2.or.day.ne.29) nn=nn+1
          cnt=cnt+1
          if(nomiss(TMIN(cnt)))then       ! for TN
            if(year.lt.BASESYEAR.or.year.gt.BASEEYEAR) then   ! outside the base years
              if(TMIN(cnt).gt.thresan90(nn)) tn90out(i,month)= tn90out(i,month)+1
            else
              do iter=1,BYRS-1         ! inside the base years
                if(TMIN(cnt).gt.thresbn90(nn,byear,iter))  tn90out(i,month)=tn90out(i,month)+1
              enddo
            endif
          else
            missncnt=missncnt+1
          endif
        enddo ! do day=1,kth

        if(year.ge.BASESYEAR.and.year.le.BASEEYEAR)then  ! inside base years
          tn90out(i,month)=tn90out(i,month)/BYRSm
        endif

        ! NOTE that the index allows at most 10 MISSING days in a month !!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if(missncnt.le.10.and.flgtn.eq.0)then   !  TN index
          temp=100./(kth-missncnt)
          tn90out(i,13)=tn90out(i,13)+tn90out(i,month)
          tn90out(i,month)=tn90out(i,month)*temp
        else
          tn90out(i,month)=MISSING
        endif
      enddo ! do month=1,12

      ! get annual index
      ! If annual TX/N is MISSING, set annual index=MISSING
      if(YNASTAT(i,3).eq.1.or.flgtn.eq.1) then
        tn90out(i,13)=MISSING
      else
        tn90out(i,13)=tn90out(i,13)*rDoY
      endif
    enddo   ! year loop

130   continue

    ! output monthly index and/or thresholds.
    if(flgtn.eq.0)then
      call save_monthly_indice(indice_type, name, tn90out)
    endif

    ! to output thresholds as well, requested by Lisa @ 2011.08.01 (Jana Sillmann wants them)
    if(save_thresholds) then
      write(threshold_name, '(a2,i2,a)') "an", TN90pLevel*100, "p"
      call save_thres_indice(indice_type, name, threshold_name , thresan90)
    endif

    return

    end subroutine TN90p

! -------------------------------------------------------------------
! Calculate TX90p
!  
! definitions are:
! TX90p: Percentage of days when TX > 90th percentile
! To avoid possible inhomogeneity across the in-base and out-base periods, 
!    the calculation for the base period requires the use of a bootstrap processure. 
!    Details are described in Zhang et al. (2004).
!
! NOTE that the index allows at most 10 MISSING days in a month !!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! input
!    none
!   (transfer data through the module COMM)

    subroutine TX90p(indice_type, name, TX90pLevel)
    use COMM
    use functions

    implicit none
    character(40), intent(in)       :: indice_type, name
    character(40)                   :: threshold_name
    real, intent(in)                :: TX90pLevel
    integer:: year, month, day, kth, cnt, nn,  missxcnt, & 
              iter, cntx, i,byear,flgtx,flg, Ky   ! loop index, count #, error indicator

    real,dimension(DoY,BYRS,BYRS-1) :: thresbx90
    real,dimension(DoY)             :: threstmp   ! temporary threshold data
    real,dimension(BYRS,DoY+2*SS)   :: txdata, &  ! extended TX data in base period
                                       txboot
    real,dimension(DoY)             :: thresax90  ! thresholds
    real,dimension(BYRS,DoY)        :: txdtmp     ! base year TX
    real,dimension(YRS,13)          :: tx90out    ! monthly index
    real :: BYRSm,rDoY,temp

    integer :: SmByear ! SYear-BaseYear

    if(Tmax_miss .and. Tmin_miss) return

    BYRSm=BYRS-1.0
    rDoY=100.0/DoY
    cnt=0
    nn=0
    txdtmp=MISSING

    ! choose TX and TN in base years, excluding 29th of Feb.
    SmByear=SYEAR-BASESYEAR
    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      nn=0
      do month=1,12
        Kth=Mon(month,Ky)
        do day=1,kth
          cnt=cnt+1
          if(year.ge.BASESYEAR.and.year.le.BASEEYEAR.and.(month.ne.2 &
            .or.day.ne.29))then
            nn=nn+1
            txdtmp(i+SmByear,nn)=TMAX(cnt)
          endif
        enddo
      enddo
      if(year.ge.BASESYEAR.and.year.le.BASEEYEAR.and.nn.ne.DoY)then
        print *,"date count error in TX10p!", nn
        stop
      endif
    enddo

    ! extended TN and TX in base years
    txdata(1,1:SS)=txdtmp(1,1)
    txdata(2:BYRS,1:SS)=txdtmp(1:BYRS-1,DoY+1-SS:DoY)
    ! out-bounds found by Imke, corrected on 2012.7.18, H.Yang @ CCRC.
    txdata(:,1+SS:DoY+SS)=txdtmp(:,1:DoY)
    txdata(BYRS,1+DoY+SS:DoY+SS*2)=txdtmp(BYRS,DoY)
    txdata(1:BYRS-1,1+DoY+SS:DoY+SS*2)=txdtmp(2:BYRS,1:SS)

    flgtx=0
    call single_threshold(txdata,TX90pLevel,threstmp,flgtx)  ! thresholds for TX
      thresax90(:)=threstmp(:)+1e-5
    if(flgtx.eq.1) then
      write(6,*) "TMAX Missing value overflow in exceedance rate"
      tx90out=MISSING
    endif

    if(Tmax_miss) flgtx=1  ! if all missing, don't output

    do i=1,BYRS   ! This part consumes most of the time, due to "threshold"...
      txboot=txdata
      nn=0
      do iter=1,BYRS
        if(iter.ne.i) then
          nn=nn+1
          if(flgtx.eq.0) then
            txboot(i,:)=txboot(iter,:)
            call single_threshold(txboot,TX90pLevel,threstmp,flgtx)
            thresbx90(:,i,nn)=threstmp(:)+1e-5
          endif
        endif
      enddo
    enddo

    if(flgtx.eq.0)then
      tx90out=0.
    endif

    cnt=0
    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      byear=year-BASESYEAR+1
      nn=0
      do month=1,12
        missxcnt=0
        Kth=Mon(month,Ky)
        do day=1,kth
          if(month.ne.2.or.day.ne.29) nn=nn+1
          cnt=cnt+1
          if(nomiss(TMAX(cnt)))then       ! for TX
            if(year.lt.BASESYEAR.or.year.gt.BASEEYEAR) then  ! outside the base years
              if(TMAX(cnt).gt.thresax90(nn)) tx90out(i,month)= tx90out(i,month)+1
            else
              do iter=1,BYRS-1       ! inside base years
                if(TMAX(cnt).gt.thresbx90(nn,byear,iter))   tx90out(i,month)=tx90out(i,month)+1
              enddo
            endif
          else
            missxcnt=missxcnt+1
          endif
        enddo ! do day=1,kth

        if(year.ge.BASESYEAR.and.year.le.BASEEYEAR)then  ! inside base years
          tx90out(i,month)=tx90out(i,month)/BYRSm
        endif

        ! NOTE that the index allows at most 10 MISSING days in a month !!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if(missxcnt.le.10.and.flgtx.eq.0)then   ! TX index
          temp=100./(kth-missxcnt)
          tx90out(i,13)=tx90out(i,13)+tx90out(i,month)
          tx90out(i,month)=tx90out(i,month)*temp
        else
          tx90out(i,month)=MISSING
        endif
      enddo ! do month=1,12

      ! get annual index
      ! If annual TX/N is MISSING, set annual index=MISSING
      if(YNASTAT(i,2).eq.1.or.flgtx.eq.1) then
        tx90out(i,13)=MISSING
      else
        tx90out(i,13)=tx90out(i,13)*rDoY
      endif
    enddo   ! year loop

130   continue

    ! output monthly index and/or thresholds.
    if(flgtx.eq.0)then
      call save_monthly_indice(indice_type, name, tx90out)
    endif

    ! to output thresholds as well, requested by Lisa @ 2011.08.01 (Jana Sillmann wants them)
    if(save_thresholds) then
      write(threshold_name, '(a2,i2,a)') "an", TX90pLevel*100, "p"
      call save_thres_indice(indice_type, name, threshold_name , thresax90)
    endif

    return

    end subroutine TX90p


! -------------------------------------------------------------------
! Calculate index WSDI
!  
! definitions are:

! WSDI: Warm speel duration index: Annual count of days with at least n consecutive days when TX > 90th percentile
! To avoid possible inhomogeneity across the in-base and out-base periods, 
!    the calculation for the base period requires the use of a bootstrap processure. 
!    Details are described in Zhang et al. (2004).
!
! NOTE that the index allows at most 10 MISSING days in a month !!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! input
!    none
!   (transfer data through the module COMM)

    subroutine WSDI_TX90p(name, TX90pLevel, cdays_span)
    use COMM
    use functions

    implicit none
    character(40), parameter        :: indice_type='WSDI'
    character(40), intent(in)       :: name
    real, intent(in)                :: TX90pLevel
    integer, intent(in)             :: cdays_span ! number n of consecutive days
    integer:: year, month, day, kth, cnt, nn,  missxcnt, & 
              iter, cntx, i,byear,flgtx,flg, Ky   ! loop index, count #, error indicator

    real,dimension(DoY,BYRS,BYRS-1) :: thresbx90
    real,dimension(DoY)             :: threstmp   ! temporary threshold data
    real,dimension(BYRS,DoY+2*SS)   :: txdata, &  ! extended TX data in base period
                                       txboot
    real,dimension(DoY)             :: thresax90  ! thresholds
    real,dimension(BYRS,DoY)        :: txdtmp     ! base year TX
    real,dimension(YRS,13)          :: tx90out    ! monthly index
    real,dimension(YRS)             :: wsdi       ! annual index
    real :: BYRSm,rDoY,temp

    integer :: SmByear ! SYear-BaseYear

    if(Tmax_miss .and. Tmin_miss) return

        BYRSm=BYRS-1.0
    rDoY=100.0/DoY
    cnt=0
    nn=0
    txdtmp=MISSING

    ! choose TX and TN in base years, excluding 29th of Feb.
    SmByear=SYEAR-BASESYEAR
    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      nn=0
      do month=1,12
        Kth=Mon(month,Ky)
        do day=1,kth
          cnt=cnt+1
          if(year.ge.BASESYEAR.and.year.le.BASEEYEAR.and.(month.ne.2 &
            .or.day.ne.29))then
            nn=nn+1
            txdtmp(i+SmByear,nn)=TMAX(cnt)
          endif
        enddo
      enddo
      if(year.ge.BASESYEAR.and.year.le.BASEEYEAR.and.nn.ne.DoY)then
        print *,"date count error in TX10p!", nn
        stop
      endif
    enddo

    ! extended TN and TX in base years
    txdata(1,1:SS)=txdtmp(1,1)
    txdata(2:BYRS,1:SS)=txdtmp(1:BYRS-1,DoY+1-SS:DoY)
    ! out-bounds found by Imke, corrected on 2012.7.18, H.Yang @ CCRC.
    txdata(:,1+SS:DoY+SS)=txdtmp(:,1:DoY)
    txdata(BYRS,1+DoY+SS:DoY+SS*2)=txdtmp(BYRS,DoY)
    txdata(1:BYRS-1,1+DoY+SS:DoY+SS*2)=txdtmp(2:BYRS,1:SS)

    flgtx=0
    call single_threshold(txdata,TX90pLevel,threstmp,flgtx)  ! thresholds for TX
      thresax90(:)=threstmp(:)+1e-5
    if(flgtx.eq.1) then
      write(6,*) "TMAX Missing value overflow in exceedance rate"
      tx90out=MISSING
    endif

    if(Tmax_miss) flgtx=1  ! if all missing, don't output

    do i=1,BYRS   ! This part consumes most of the time, due to "threshold"...
      txboot=txdata
      nn=0
      do iter=1,BYRS
        if(iter.ne.i) then
          nn=nn+1
          if(flgtx.eq.0) then
            txboot(i,:)=txboot(iter,:)
            call single_threshold(txboot,TX90pLevel,threstmp,flgtx)
            thresbx90(:,i,nn)=threstmp(:)+1e-5
          endif
        endif
      enddo
    enddo

    if(flgtx.eq.0)then
      tx90out=0.
    endif

    cnt=0
    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      byear=year-BASESYEAR+1
      nn=0
      do month=1,12
        missxcnt=0
        Kth=Mon(month,Ky)
        do day=1,kth
          if(month.ne.2.or.day.ne.29) nn=nn+1
          cnt=cnt+1
          if(nomiss(TMAX(cnt)))then       ! for TX
            if(year.lt.BASESYEAR.or.year.gt.BASEEYEAR) then  ! outside the base years
              if(TMAX(cnt).gt.thresax90(nn)) tx90out(i,month)= tx90out(i,month)+1
            else
              do iter=1,BYRS-1       ! inside base years
                if(TMAX(cnt).gt.thresbx90(nn,byear,iter))   tx90out(i,month)=tx90out(i,month)+1
              enddo
            endif
          else
            missxcnt=missxcnt+1
          endif
        enddo ! do day=1,kth

        if(year.ge.BASESYEAR.and.year.le.BASEEYEAR)then  ! inside base years
          tx90out(i,month)=tx90out(i,month)/BYRSm
        endif

        ! NOTE that the index allows at most 10 MISSING days in a month !!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if(missxcnt.le.10.and.flgtx.eq.0)then   ! TX index
          temp=100./(kth-missxcnt)
          tx90out(i,13)=tx90out(i,13)+tx90out(i,month)
          tx90out(i,month)=tx90out(i,month)*temp
        else
          tx90out(i,month)=MISSING
        endif
      enddo ! do month=1,12

      ! get annual index
      ! If annual TX/N is MISSING, set annual index=MISSING
      if(YNASTAT(i,2).eq.1.or.flgtx.eq.1) then
        tx90out(i,13)=MISSING
      else
        tx90out(i,13)=tx90out(i,13)*rDoY
      endif
    enddo   ! year loop

130   continue

    ! get WSDI
    cnt=0
    wsdi=0.

    do i=1,YRS
      cntx=0
      nn=0
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      do month=1,12
        Kth=Mon(month,Ky)
        do day=1,kth
          if(month.ne.2.or.day.ne.29) nn=nn+1
          cnt=cnt+1
          if(TMAX(cnt).gt.thresax90(nn).and.nomiss(TMAX(cnt))) then
            cntx=cntx+1
            if(month.eq.12.and.day.eq.31.and.cntx.ge.cdays_span)   wsdi(i)=wsdi(i)+cntx
          elseif(cntx.ge.6)then
            wsdi(i)=wsdi(i)+cntx
            cntx=0
          else
            cntx=0
          endif
        enddo  ! day
      enddo    ! month
      if(YNASTAT(i,2).eq.1) wsdi(i)=MISSING
    enddo      ! year

140   continue

! output annual index

    if(flgtx.eq.0) then
      call save_annual_folder(indice_type, name, wsdi)
    endif

    return

    end subroutine WSDI_TX90p

! -------------------------------------------------------------------
! Calculate index CSDI
!  
! definitions are:
! CSDI: Cold speel duration index: Annual count of days with at least n consecutive days when TN < 10th percentile
! To avoid possible inhomogeneity across the in-base and out-base periods, 
!    the calculation for the base period requires the use of a bootstrap processure. 
!    Details are described in Zhang et al. (2004).
!
! NOTE that the index allows at most 10 MISSING days in a month !!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! input
!    none
!   (transfer data through the module COMM)

    subroutine CSDI_TN10p(name, TN10pLevel, cdays_span)
    use COMM
    use functions

    implicit none
    character(40), parameter        :: indice_type='CSDI'
    character(40), intent(in)       :: name
    real, intent(in)                :: TN10pLevel
    integer, intent(in)             :: cdays_span        ! number n of consecutive days
    integer:: year, month, day, kth, cnt, nn, missncnt, & 
              iter, cntn,i,byear,flgtn,flg, Ky           ! loop index, count #, error indicator

    real,dimension(DoY,BYRS,BYRS-1) :: thresbn10
    real,dimension(DoY)             :: threstmp          ! temporary threshold data
    real,dimension(BYRS,DoY+2*SS)   :: tndata, tnboot    ! extended TX and TN data in base period
    real,dimension(DoY)             :: thresan10         ! thresholds
    real,dimension(BYRS,DoY)        :: tndtmp            ! base year TN.
    real,dimension(YRS,13)          :: tn10out
    real,dimension(YRS)             :: csdi              ! annual index
    real :: BYRSm,rDoY,temp

    integer :: SmByear ! SYear-BaseYear

    if(Tmax_miss .and. Tmin_miss) return

    BYRSm=BYRS-1.0
    rDoY=100.0/DoY
    cnt=0
    nn=0
    tndtmp=MISSING

    ! choose TX and TN in base years, excluding 29th of Feb.
    SmByear=SYEAR-BASESYEAR
    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      nn=0
      do month=1,12
        Kth=Mon(month,Ky)
        do day=1,kth
          cnt=cnt+1
          if(year.ge.BASESYEAR.and.year.le.BASEEYEAR.and.(month.ne.2 &
            .or.day.ne.29))then
            nn=nn+1
            tndtmp(i+SmByear,nn)=TMIN(cnt)
          endif
        enddo
      enddo
      if(year.ge.BASESYEAR.and.year.le.BASEEYEAR.and.nn.ne.DoY)then
        print *,"date count error in TX10p!", nn
        stop
      endif
    enddo

    ! extended TN in base years
    tndata(1,1:SS)=tndtmp(1,1)  ! i=1
    tndata(2:BYRS,1:SS)=tndtmp(1:BYRS-1,DoY+1-SS:DoY) ! i /=1
    tndata(:,1+SS:DoY+SS)=tndtmp(:,1:DoY)
    tndata(BYRS,1+DoY+SS:DoY+SS*2)=tndtmp(BYRS,DoY)
    tndata(1:BYRS-1,1+DoY+SS:DoY+SS*2)=tndtmp(2:BYRS,1:SS)

    flgtn=0
    call single_threshold(tndata,TN10pLevel,threstmp,flgtn)  ! thresholds for TN
      thresan10(:)=threstmp(:)-1e-5
    if(flgtn.eq.1) then
      write(6,*) "TMIN Missing value overflow in exceedance rate"
      tn10out=MISSING
    endif

    if(Tmin_miss) flgtn=1

    !$OMP parallel do default(shared) private(i,nn,txboot,tnboot,iter,threstmp,flg)
    do i=1,BYRS   ! This part consumes most of the time, due to "threshold"...
      tnboot=tndata
      nn=0
      do iter=1,BYRS
        if(iter.ne.i) then
          nn=nn+1
            if(flgtn.eq.0) tnboot(i,:)=tnboot(iter,:)

          if(flgtn.eq.0) then
            call single_threshold(tnboot,TN10pLevel,threstmp,flgtn)
              thresbn10(:,i,nn)=threstmp(:)-1e-5
          endif
        endif
      enddo
    enddo

    if(flgtn.eq.0)then
      tn10out=0.
    endif

    cnt=0
    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      byear=year-BASESYEAR+1
      nn=0
      do month=1,12
        missncnt=0
        Kth=Mon(month,Ky)
        do day=1,kth
          if(month.ne.2.or.day.ne.29) nn=nn+1
          cnt=cnt+1
          if(nomiss(TMIN(cnt)))then       ! for TN
            if(year.lt.BASESYEAR.or.year.gt.BASEEYEAR) then   ! outside the base years
              if(TMIN(cnt).lt.thresan10(nn)) tn10out(i,month)= tn10out(i,month)+1
            else
              do iter=1,BYRS-1         ! inside the base years
                if(TMIN(cnt).lt.thresbn10(nn,byear,iter))  tn10out(i,month)=tn10out(i,month)+1
              enddo
            endif
          else
            missncnt=missncnt+1
          endif
        enddo ! do day=1,kth

        if(year.ge.BASESYEAR.and.year.le.BASEEYEAR)then  ! inside base years
          tn10out(i,month)=tn10out(i,month)/BYRSm
        endif

        ! NOTE that the index allows at most 10 MISSING days in a month !!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if(missncnt.le.10.and.flgtn.eq.0)then   !  TN index
          temp=100./(kth-missncnt)
          tn10out(i,13)=tn10out(i,13)+tn10out(i,month)
          tn10out(i,month)=tn10out(i,month)*temp
        else
          tn10out(i,month)=MISSING
        endif
      enddo ! do month=1,12

      ! get annual index
      ! If annual TX/N is MISSING, set annual index=MISSING
      if(YNASTAT(i,3).eq.1.or.flgtn.eq.1) then
        tn10out(i,13)=MISSING
      endif
    enddo   ! year loop

130   continue

    ! get CSDI
    cnt=0
    csdi=0.

    do i=1,YRS
      cntn=0
      nn=0
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      do month=1,12
        Kth=Mon(month,Ky)
        do day=1,kth
          if(month.ne.2.or.day.ne.29) nn=nn+1
          cnt=cnt+1
          if(TMIN(cnt).lt.thresan10(nn).and.nomiss(TMIN(cnt))) then
            cntn=cntn+1
            if(month.eq.12.and.day.eq.31.and.cntn.ge.6)   csdi(i)=csdi(i)+cntn
          elseif(cntn.ge.6)then
            csdi(i)=csdi(i)+cntn
            cntn=0
          else
            cntn=0
          endif
        enddo  ! day
      enddo    ! month
      if(YNASTAT(i,3).eq.1) csdi(i)=MISSING  ! If annual TX/N is MISSING, set annual index=MISSING.
    enddo      ! year

140   continue

    ! output annual index
    if(flgtn.eq.0)then
      call save_annual_folder(indice_type, name, csdi)
    endif

    return

    end subroutine CSDI_TN10p

! -------------------------------------------------------------------
! ---------- play all ETCCDI indices subroutines ----------
! Calculate index TX10p, TN10p, TX50p, TN50p, TX90p, TN90p, WSDI, CSDI
!  and save thresholds if requested.
!  
! definitions are:
! TN10p: Percentage of days when TN < 10th percentile.
! TX10p: Percentage of days when TX < 10th percentile.
! TN50p: Percentage of days when TN > 50th percentile
! TX50p: Percentage of days when TX > 50th percentile
! TN90p: Percentage of days when TN > 90th percentile
! TX90p: Percentage of days when TX > 90th percentile
! WSDI: Warm speel duration index: Annual count of days with at least 6 consecutive days when TX > 90th percentile
! CSDI: Cold speel duration index: Annual count of days with at least 6 consecutive days when TN < 10th percentile
! To avoid possible inhomogeneity across the in-base and out-base periods, 
!    the calculation for the base period requires the use of a bootstrap processure. 
!    Details are described in Zhang et al. (2004).
!
! NOTE that the index allows at most 10 MISSING days in a month !!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! input
!    none
!   (transfer data through the module COMM)

    subroutine exec_all_temperature_extremes
    use COMM
    use functions
    implicit none

    character(40), parameter :: name_TN10p='TN10p', name_TN50p='TN50p', name_TN90p='TN90p', &
                                name_TX10p='TX10p', name_TX50p='TX50p', name_TX90p='TX90p', &
                                name_CSDI='CSDI', name_WSDI='WSDI'

    ! TN
    call TN10p(name_TN10p, 0.1)
    call TN90p(name_TN50p, name_TN50p, 0.5)
    call TN90p(name_TN90p, name_TN90p, 0.9)
    call CSDI_TN10p(name_CSDI, 0.1, 6)

    ! TX
    call TX10p(name_TX10p, 0.1)
    call TX90p(name_TX50p, name_TX50p, 0.5)
    call TX90p(name_TX90p, name_TX90p, 0.9)
    call WSDI_TX90p(name_WSDI, 0.9, 6)

   return
   end subroutine exec_all_temperature_extremes
