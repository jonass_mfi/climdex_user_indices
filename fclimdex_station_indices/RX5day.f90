! ----------------------------------------------------------------------
! Calculate index Rx1day, Rx5day.
!
! definitions are:
! Rx1day, Monthly maximum 1-day precipitation.
! Rx5day, Monthly maximum consecutive 5-day precipitation. 
!
! input
!    none
!   (transfer data through the module COMM)
!
    subroutine RX5day(name, cdays_span)
    use COMM
    use functions

    implicit none
    character(40)             :: indice_type
    character(40), intent(in) :: name
    integer, intent(in)       :: cdays_span
    integer :: year, month, day,cnt,k,kth,i, Ky ! loop index and/or count #.
    real    :: rx(YRS,13), r5prcp  ! index to be calculated

    if(Prcp_miss) return

    if(cdays_span.eq.1) then
      indice_type='Rx1day'
    else
      indice_type='Rx5day'
    endif

    cnt=0
    rx=MISSING
    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      do month=1,12
        Kth=Mon(month,Ky)
        do day=1,kth
          cnt=cnt+1
          ! get Rx5day
          if(cdays_span.gt.1..and.cnt.ge.cdays_span) then
            r5prcp=0.
            do k=cnt-(cdays_span-1),cnt
              if(nomiss(PRCP(k))) then
                r5prcp=r5prcp+PRCP(k)
              endif
            enddo
          else
            r5prcp=MISSING
          endif
          if(cdays_span.eq.1..and.nomiss(PRCP(cnt)).and.(ismiss(rx(i,month))  &   ! Rx1day
            .or.PRCP(cnt).gt.rx(i,month))) then
            rx(i,month)=PRCP(cnt)
          endif
          if(cdays_span.gt.1..and.nomiss(PRCP(cnt)).and.r5prcp.gt.rx(i,month)) then  ! Rx5day
            rx(i,month)=r5prcp
          endif
        enddo  ! loop for day

        if(MNASTAT(i,month,1).eq.1) then  ! If monthly PRCP is MISSING, set monthly index=MISSING 
          rx(i,month)=MISSING
        endif

        if(nomiss(rx(i,month)).and.(ismiss(rx(i,13))    &  ! annual Rx1day
          .or.rx(i,month).gt.rx(i,13))) then
          rx(i,13)=rx(i,month)
        endif
      enddo     ! loop for month
      
      ! check if annual PRCP is MISSING
      ! if yes, set annual index=MISSING
      if(YNASTAT(i,1).eq.1) then
        rx(i,13)=MISSING
      endif
    enddo     ! loop for year

    ! output the index
    call save_monthly_indice(indice_type, name, rx)

    return
    end subroutine RX5day


! ---------- play all ETCCDI indices subroutines ----------
    subroutine exec_all_monthly_max_precipitation
    use COMM
    use functions

    implicit none
    character(40), parameter :: name_Rx1day='Rx1day', name_Rx5day='Rx5day'

    call Rx5day(name_Rx1day, 1)
    call Rx5day(name_Rx5day, 5)

    return
    end subroutine exec_all_monthly_max_precipitation
