! -----------------------------------------------------------
! common module
!    Global parameters/variables used by all subroutines.
! -----------------------------------------------------------
    MODULE COMM		
      IMPLICIT NONE 
      integer,parameter  :: DP=kind(1.0)	!  single precision
      integer,parameter  :: MaxYear=500, DoY=365, &  ! DoY=Day_of_Year
                            N_index=40       ! total index #, some might not be calculated
      real(DP), parameter:: MISSING = -99.9

      character(200) :: StnID,ofile_base,ifile,odir
      integer(4)    :: STDSPAN, BASESYEAR, BASEEYEAR, &
              SYEAR, EYEAR, TOT, YRS, BYRS, WINSIZE, SS, &
              PRCPNN, &    ! User-defined precipitation amount in millimetres for index "Rnnmm"
              iend         ! whether finish reading all stations (1= yes.)
      real(DP),dimension(MaxYear*DoY):: PRCP,Tmax,Tmin  ! prcp and temperature data
      integer,dimension(12,2)        :: Mon   ! we use new Mon in palce of old Mon and Monleap
      real(DP)   :: LATITUDE,  &           ! latitude of the station
                    NoMissingThreshold     ! Fraction of data allowed to be missing to calculate reliable index values
      integer(4) :: YMD(MaxYear*DoY,3), &  ! hold date - year month day for all parts
                    MNASTAT(MaxYear,12,3),YNASTAT(MaxYear,3) !  missing values monthly/annual.
      integer    :: ID_log,ID_ifile,IDsave,ID_para,ID_inf, ID_ind  ! unit ID for I/O files
      character(200) :: folder(N_index),OS,Oname,log_file,ipt_dir,opt_dir,data_dir,inf_file,para_file,ind_file
      character(250) :: ofile(N_index)  ! output file names (including dir)
      character(1)  :: sub2(2),sub
      logical       :: Tmax_miss,Tmin_miss,Prcp_miss, &  ! check if ALL data is missing...
                       save_thresholds, &           ! whether the threshold data should be saved.
                       cal_EHI                      ! Whether to calculate EHI

    ! User indices - to be allocated
      character(40), dimension(:, :), allocatable :: indices
      real, dimension(:, :), allocatable :: indices_params

    ! Number of days in each month (both non-leap and leap years)
      data MON    /31,28,31,30,31,30,31,31,30,31,30,31, &
                   31,29,31,30,31,30,31,31,30,31,30,31/
      data WINSIZE/5/
      data sub2/'/','\'/          ! used for different OS system - Windows or Linux

    ! List of indices to calculate (and/or folders to be created)
      data folder/'FD','SU','ID','TR','GSL','TXx','TXn','TNx','TNn','DTR', &
            'R10mm','R20mm','Rnnmm','SDII','Rx1day','Rx5day','CDD','CWD','R95p','R99p', &
            'PRCPTOT','TX10p','TX50p','TX90p','TN10p','TN50p','TN90p','prcpQC','tempQC','NASTAT', &
            'WSDI','CSDI','ETR','R85pTOT','R95pTOT','R99pTOT','R85p','thresTemp','thresPrcp','EHI'/

    END MODULE COMM


!-----------------------------------------------------------
! MODULE: functions
!  contains three functions
! ----------------------------------------------------------
!
 module functions
      use COMM !, only:DP, MISSING ! old version of PGF90 has problem with 'only'...
      real(DP) :: rmiss
      public   :: ismiss,nomiss, leapyear

  contains

!  return TRUE if 'a' is MISSING
!         FALSE if 'a' is not MISSING

      logical function ismiss(a)
      real(DP) :: a
      rmiss=Missing+1.
      if(a.gt.rmiss) then
        ismiss=.FALSE.
      else
        ismiss=.TRUE.
      endif
      end function ismiss


!  return TRUE if a is not MISSING
!         FALSE if a is MISSING

      logical function nomiss(a)
      real(DP) :: a
      rmiss=Missing+1.
      if(a.lt.rmiss) then
        nomiss=.FALSE.
      else
        nomiss=.TRUE.
      endif
      end function nomiss


!  ----------------------------------------------------
!  check if 'iyear' is a leap year
!   if iyear is non-leap year, returns 0
!   if iyear is leap year, returns 1
!
      integer function leapyear(iyear)
      integer:: iyear

      if(mod(iyear,400)==0) then
        leapyear=1
      else
        if(mod(iyear,100)==0) then
          leapyear=0
        else
          if(mod(iyear,4)==0) then
            leapyear=1
          else
            leapyear=0
          endif
        endif
      endif

      end function leapyear

 end module functions
