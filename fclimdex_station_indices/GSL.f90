! subroutines used by fclimdex_station, to calculate index: GSL.
!
! definitions:
! GSL: Growing Season Length: Annual (1st Jan to 31st Dec in Northern Hemisphere (NH), 
!  1st July to 30th June in Southern Hemisphere (SH)) count between first span of at least 6 days 
!  with daily mean temperature TG>5 degree and first span after July 1st (Jan 1st in SH) of 6 days with TG<5 degree. 
!
! input
!    none
!   (transfer data through the module COMM)

    subroutine GSL(name, TGmin, cdays_sspan, TGmax, cdays_espan)
    use COMM
    use functions

    implicit none
    character(40), parameter  :: indice_type='GSL'
    character(40), intent(in) :: name
    real, intent(in)          :: TGmin, TGmax
    integer, intent(in)       :: cdays_sspan, cdays_espan
    integer      :: year,cnt,kth,month,day,marks,marke,i, Ky ! loop index and/or count #
    real         :: TG,oout(YRS),strt(YRS),ee(YRS) ! annual index to be calculated

    if(Tmax_miss .and. Tmin_miss) return

    strt=MISSING
    ee=MISSING
    cnt=0

    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      marks=0
      marke=0
      do month=1,6
         Kth=Mon(month,Ky)
        do day=1,kth
          cnt=cnt+1
          if(YMD(cnt,1)*10000+YMD(cnt,2)*100+YMD(cnt,3).ne.  &
            year*10000+month*100+day) then
            print*, 'date count ERROR in GSL!'
            print*, YMD(cnt,1)*10000+YMD(cnt,2)*100+YMD(cnt,3), &
                   year*10000+month*100+day
            stop
          endif
          if(nomiss(TMAX(cnt)).and.nomiss(TMIN(cnt))) then
            TG=(TMAX(cnt)+TMIN(cnt))/2.
          else
            TG=MISSING
          endif
          
          if(LATITUDE.lt.0) then             ! South Hemisphere
            if(nomiss(TG).and.TG.lt.TGmax) then
              marke=marke+1
            else
              marke=0
            endif
            if(marke.ge.cdays_espan.and.i.gt.1.and.ismiss(ee(i-1)))then
                  ee(i-1)=cnt-cdays_espan
            endif

            if(ismiss(ee(i-1)).and.month.eq.6.and.day.eq.kth) then !Growing season never end
              ee(i-1)=cnt
            endif
            
          else                                ! North Hemisphere
            if(nomiss(TG).and.TG.gt.TGmin) then
              marks=marks+1
            else
              marks=0
            endif
            if(marks.ge.cdays_sspan.and.ismiss(strt(i)))then
              strt(i)=cnt-cdays_sspan
            endif
          endif
          
        enddo
      enddo 

      marks=0
      marke=0
      do month=7,12
        do day=1,MON(month,1) ! Here it doesn't matter whether it's leap year. 
          cnt=cnt+1
          if(nomiss(TMAX(cnt)).and.nomiss(TMIN(cnt))) then
            TG=(TMAX(cnt)+TMIN(cnt))/2.
          else
            TG=MISSING
          endif
          if(LATITUDE.lt.0) then           ! South Hemisphere
            if(nomiss(TG).and.TG.gt.TGmin) then
              marks=marks+1
            else
              marks=0
            endif
            if(marks.ge.cdays_sspan.and.ismiss(strt(i)))then
              strt(i)=cnt-cdays_sspan
            endif
          else                            ! North Hemisphere
            if(nomiss(TG).and.TG.lt.TGmax) then
              marke=marke+1
            else
              marke=0
            endif
            if(marke.ge.cdays_espan.and.ismiss(ee(i)))then
              ee(i)=cnt-cdays_espan
            endif

            if (ismiss(ee(i)).and.month.eq.12.and.day.eq.MON(12,1)) then
               ee(i)=cnt
            endif
          endif
        enddo
      enddo
    enddo

    do i=1,YRS
      year=i+SYEAR-1
      if(nomiss(strt(i)).and.nomiss(ee(i)).and.	&
        YNASTAT(i,2).ne.1.and.YNASTAT(i,3).ne.1)then
        oout(i)=ee(i)-strt(i)
      elseif(ismiss(strt(i)).or.ismiss(ee(i))) then
        oout(i)=0.
      endif
      if(YNASTAT(i,2).eq.1.or.YNASTAT(i,3).eq.1) oout(i)=MISSING    ! at most 15 MISSING days in a year!!!!!!!!!!!!!!!!!!!!!!!
    enddo

    ! output the index
    call save_annual_folder(indice_type, name, oout)  ! GSL

    return
    end subroutine GSL 

! ---------- play all ETCCDI indices subroutines ----------
    subroutine exec_growing_season_length
    use COMM
    use functions

    implicit none
    character(40), parameter :: name_GSL='GSL'

    call GSL(name_GSL, 5, 6, 5, 6)

    end subroutine exec_growing_season_length