! ----------------------------------------------------------------
! get index R10mm, R20mm, Rnnmm
!
! definitions are:
! Rnnmm: Annual count of days when PRCP≥ nnmm, nn is a user defined threshold.
!
! input
!    none
!   (transfer data through the module COMM)
!
    subroutine Rnnmm(indice_type, name, PRCPNNLevel)
    use COMM
    use functions !, only : leapyear

    implicit none
    character(40), intent(in) :: indice_type, name
    integer, intent(in)       :: PRCPNNLevel
    integer                   :: year,month,day,kth,cnt,i,k, Ky ! loop index and/or count #.
    real                      :: oout(YRS)  ! annual index to be calculated

    if(Prcp_miss) return

    ! get the annual index
    cnt=0
    oout=0.
    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      do month=1,12
        Kth=Mon(month,Ky)
        do day=1,kth
          cnt=cnt+1
          if(PRCP(cnt).ge.PRCPNNLevel) oout(i)=oout(i)+1.
        enddo
      enddo
    enddo  ! loop for year

    ! If annual PRCP is MISSING, also set annual index=MISSING
    do i=1,YRS
      if(YNASTAT(i,1).eq.1) then
         oout(i)=MISSING
      endif
    enddo

    ! output the index
    call save_annual_folder(indice_type, name, oout)

    return
    end subroutine Rnnmm

! ----------------------------------------------------------------
! get index SDII
!
! definitions are:
! SDII:  Simple pricipitation intensity index (on wet days, w PRCP ≥ 1mm).
!
! input
!    none
!   (transfer data through the module COMM)
!
    subroutine SDII
    use COMM
    use functions !, only : leapyear

    implicit none
    character(40), parameter :: indice_type='SDII', name='SDII'
    integer      :: year,month,day,kth,cnt,nn,i,k, Ky ! loop index and/or count #.
    real         :: oout(YRS)  ! annual index to be calculated

    if(Prcp_miss) return

    ! get the annual index
    cnt=0
    oout=0.
    do i=1,YRS
      nn=0
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      do month=1,12
        Kth=Mon(month,Ky)
        do day=1,kth
          cnt=cnt+1
          if(PRCP(cnt).ge.1.) then
            oout(i)=oout(i)+PRCP(cnt)
            nn=nn+1
          endif
        enddo
      enddo
      if(nn.gt.0) then
        oout(i)=oout(i)/nn
      endif
    enddo  ! loop for year

    ! If annual PRCP is MISSING, also set annual index=MISSING
    do i=1,YRS
      if(YNASTAT(i,1).eq.1) then
         oout(i)=MISSING
      endif
    enddo

    ! output the index
    call save_annual_folder(indice_type, name, oout)

    return
    end subroutine SDII


! ---------- play all ETCCDI indices subroutines ----------
    subroutine exec_all_precipitation
    use COMM
    use functions !, only : leapyear

    character(40), parameter :: name_R10mm='R10mm', name_R20mm='R20mm', name_Rnnmm='Rnnmm'

    call Rnnmm(name_R10mm, name_R10mm, 10)
    call Rnnmm(name_R20mm, name_R20mm, 20)
    call Rnnmm(name_Rnnmm, name_Rnnmm, PRCPNN)
    call SDII

    end subroutine exec_all_precipitation
