! ------------------------------------------------------------------
! Calculate index R85p, R95p, R99p, PRCPTOT, R85pTOT, R95pTOT, R99pTOT
!
! definitions are:
! R85p: Annual total PRCP when RR > 85p (on a wet day RR ≥ 1.0mm).
! R95p: Annual total PRCP when RR > 95p (on a wet day RR ≥ 1.0mm).
! R99p: Annual total PRCP when RR > 99p (on a wet day RR ≥ 1.0mm)
! R85pTOT=(R85p/PRCPTOT)*100.
! R95pTOT=(R95p/PRCPTOT)*100.
! R99pTOT=(R99p/PRCPTOT)*100.
!
! input
!    none
!   (transfer data through the module COMM)
!
    subroutine R95p(name, percentile, min_rr)
    use COMM
    use functions

    implicit none
    character(40), parameter  :: indice_type='R95p'
    character(40), intent(in) :: name
    character(40)             :: name_total
    real(DP), intent(in)      :: percentile
    integer, intent(in)       :: min_rr
    integer                   :: year, month, day, kth,cnt,leng,i, Ky ! loop index and/or count #.
    real(DP),dimension(YRS)   :: oout, prcpout, ooutTOT  ! index to be calculated
    real(DP),dimension(TOT)   :: prcptmp  ! hold selected PRCP on wet days in base period
    real(DP)                  :: rtmp  ! percentile results

    name_total = name // 'TOT'

    if(Prcp_miss) return

    ! get wet days PRCP during base period - prcptmp
    cnt=0
    leng=0
    prcptmp=MISSING
    ! The year loop should be changed because it only needs to cover base years !!!!!!!!!!!!!!!!!!!!
    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      do month=1,12
        kth=Mon(month,Ky)
        do day=1,kth
          cnt=cnt+1
          if(year.ge.BASESYEAR.and.year.le.BASEEYEAR.and. &
            nomiss(PRCP(cnt)).and.PRCP(cnt).ge.min_rr)then
            leng=leng+1
            prcptmp(leng)=PRCP(cnt)  ! wet days PRCP during base period
          endif
        enddo
      enddo
    enddo

    call single_percentile(prcptmp,leng,percentile,rtmp)

    ! get R95p
    cnt=0
    oout=0.
    prcpout=0.
    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      do month=1,12
        kth=Mon(month,Ky)
        do day=1,kth
          cnt=cnt+1
          if(PRCP(cnt).ge.min_rr.and.nomiss(PRCP(cnt)))then
            prcpout(i)=prcpout(i)+PRCP(cnt)
            ! Should we move the nomiss(R95) to before the beginning of the loop?  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if(nomiss(rtmp).and.PRCP(cnt).gt.rtmp) oout(i)=oout(i)+PRCP(cnt)
          endif
        enddo
      enddo
        
      ! check if annual PRCP is MISSING
      ! if yes, set index=MISSING
      if(YNASTAT(i,1).eq.1) then
        prcpout(i)=MISSING
        oout(i)=MISSING
      endif
    enddo

    ! get R95pTOT:
    ooutTOT=MISSING
    where(prcpout/=MISSING .and. oout/=MISSING .and. prcpout/=0.) ooutTOT=oout/prcpout*100.

    ! output the index
    call save_annual_folder(indice_type, name, oout)

    call save_annual_folder(indice_type, name_total, ooutTOT)

    if(save_thresholds) call save_thres_PRCP_indice(indice_type, name, rtmp)

    return
    end subroutine R95p

! ------------------------------------------------------------------
! Calculate index R85p, R95p, R99p, PRCPTOT, R85pTOT, R95pTOT, R99pTOT
!
! definitions are:
! PRCPTOT: Annual total precipitation in wet days (RR ≥ 1.0mm).
!
! input
!    none
!   (transfer data through the module COMM)
!
    subroutine PRCPTOT(name, min_rr)
    use COMM
    use functions

    implicit none
    character(40), parameter  :: indice_type='PRCPTOT'
    character(40), intent(in) :: name
    integer, intent(in)       :: min_rr   ! The minimum precipitation in mm to call it a wet day
    integer                   :: year, month, day, kth,cnt,leng,i, Ky ! loop index and/or count #.
    real(DP),dimension(YRS)   :: prcpout  ! index to be calculated
    real(DP),dimension(TOT)   :: prcptmp  ! hold selected PRCP on wet days in base period
    real(DP)                  :: rtmp     ! percentile results

    if(Prcp_miss) return

    ! get wet days PRCP during base period - prcptmp
    cnt=0
    leng=0
    prcptmp=MISSING
    ! The year loop should be changed because it only needs to cover base years !!!!!!!!!!!!!!!!!!!!
    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      do month=1,12
        kth=Mon(month,Ky)
        do day=1,kth
          cnt=cnt+1
          if(year.ge.BASESYEAR.and.year.le.BASEEYEAR.and. &
            nomiss(PRCP(cnt)).and.PRCP(cnt).ge.min_rr)then
            leng=leng+1
            prcptmp(leng)=PRCP(cnt)  ! wet days PRCP during base period
          endif
        enddo
      enddo
    enddo

    cnt=0
    prcpout=0.
    do i=1,YRS
      year=i+SYEAR-1
      Ky=leapyear(year)+1
      do month=1,12
        kth=Mon(month,Ky)
        do day=1,kth
          cnt=cnt+1
          if(PRCP(cnt).ge.min_rr.and.nomiss(PRCP(cnt))) then
            prcpout(i)=prcpout(i)+PRCP(cnt)
          endif
        enddo
      enddo
        
      ! check if annual PRCP is MISSING
      ! if yes, set index=MISSING
      if(YNASTAT(i,1).eq.1) then
        prcpout(i)=MISSING
      endif
    enddo


    ! output the index
    call save_annual_folder(indice_type, name, prcpout)

    return
    end subroutine PRCPTOT 

! ---------- play all ETCCDI indices subroutines ----------
    subroutine exec_all_extremes_precipitation
    use COMM
    use functions

    character(40), parameter :: name_R85p='R85p', name_R95p='R95p', name_R99p='R99p', &
                                name_PRCPTOT='PRCPTOT'

    call R95p(name_R85p, 0.85, 1)
    call R95p(name_R95p, 0.95, 1)
    call R95p(name_R99p, 0.99, 1)
    call PRCPTOT(name_PRCPTOT, 1)

    end subroutine exec_all_extremes_precipitation
