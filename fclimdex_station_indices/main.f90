! ------------------------------------------------------------------------
! FClimDex project
!
! Fortran implementation of RClimDex (which in turn is an implementation
! in R of the Microsoft Excel ClimDex program) for calculating
! indices of climate extremes.
!
! FClimDex and RClimDex were originally written and maintained at the
! Climate Research Branch of Meteorological Service of Canada.
!
! Based on the version provided by Lisa Alexander @ 2011.4.11
! To simplify, modify and improve the code by H.Yang from 2011.4.12
!
! This version is for station data only -- no netcdf format.
!
! How-to-Run and Change History can be found in the "readme.txt"
!
! last modified @ 2012-07-19
!-----------------------------------------------------------------------


!-----------------------------------------------------------------------
! **********     Main program starts from here	 ********************* !
!
   program Fclimdex
   use COMM
   implicit none
   integer :: OMP_GET_NUM_THREADS,OMP_GET_THREAD_NUM,IDcpu, &  ! used only for OpenMP
              ID,status, &         ! unit ID for I/O, and error status (0 means OK)
              i
   integer(4)       :: stnnum      ! # of stations
   double precision :: tfrom,tto   ! current time in seconds
   namelist /input/ ipt_dir,opt_dir,para_file,data_dir,inf_file,log_file,OS, &
                    save_thresholds,NoMissingThreshold,cal_EHI,ind_file
   character(200) :: indice
   character(40), parameter :: name_Rnnmm='Rnnmm'


! This is used for OpenMP run, to print some information on screen.
!$OMP parallel private(IDcpu);  IDcpu=omp_get_thread_num();   print*,'OpenMP ID #',IDcpu,'of ',omp_get_num_threads()
!$OMP end parallel

 ! default values for the run, can be re-set in the "input.nml" file.
    save_thresholds=.false.  ! whether to save the thresholds
    NoMissingThreshold=0.85  ! default value, suggested by Lisa
    cal_EHI=.false.          ! whether to calculate Heat Wave Indices

    SS=int(WINSIZE/2)

    call getID(ID)        ! open namelist file
    open (ID, file="input.nml",status='OLD')
    read(ID,nml=input)
    close(ID)
    if(OS/='windows'.and.OS/='linux') stop 'Error in input.nml: OS wrong !'

    call getID(ID_log)    ! log file
    open (ID_log, file=trim(opt_dir)//trim(log_file),IOSTAT=status)
    if(status.ne.0) then
      close(ID_log)
      stop 'ERROR in inp.nml: the opt_dir does NOT exist, or permission wrong !'
    endif

    call check_folders  ! check if folder exists, if not, create it.
     ! whether to open a single index file for all stations

    stnnum=1

    call get_time(0,ID_log,tfrom)   ! get initial time for this running

    if(ind_file.ne."") call extract_user_indices

    ! start loop for each station: read infile, params, calculate indices...
    do
      call read_file(stnnum)
      if(iend==1) exit   ! finish all stations, running is over...

      do i=1,N_index
        ofile(i)=trim(opt_dir)//trim(folder(i))//sub//trim(Oname)//trim(folder(i))//'.txt'
      enddo
      if(stnnum==1) then
        print*,'The output names look like this:'
        print*,trim(ofile(1))
        print*,'If you"re not satisfied, please change line 69 in the "main.f90" ...'
        print*,''
      endif

      BYRS=BASEEYEAR-BASESYEAR+1   ! Base year #

      call qc   ! Quality control of data, and check if all data are missing
      if(Tmax_miss .and. Tmin_miss .and. Prcp_miss) then
        print*,'WARNING: all data are missing, this station is ignored ...'
        write(ID_log,'(a20,a)') stnID,': all data are MISSING ....'
        stnnum=stnnum+1
        cycle
      endif

      ! We calculate standard indices
      call exec_all_daily_temperature         ! FD, SU, ID, TR
      call exec_growing_season_length         ! GSL
      call TXX                                ! TXx, TXn, TNx, TNn, DTR, ETR
      call exec_all_precipitation             ! R10mm, R20mm, Rnnmm, SDII
      call exec_all_monthly_max_precipitation ! Rx1day, Rx5day
      call exec_all_precipitation_spell       ! CDD, CWD
      call exec_all_extremes_precipitation    ! R95p, R99p, PRCPTOT, R95pTOT, R99pTOT
      call exec_all_temperature_extremes      ! TX10p, TN10p, TX50p, TN50p, TX90p, TN90p, save_thresholds, WSDI, CSDI

      if(cal_EHI) call HeatWave ! EHIa, EHIs, EHF

      ! Then we calculate user indices
      if(size(indices).gt.0) then
        do i=0,size(indices,1)
          select case (indices(i,1))
            case ('SU')
              call SU(indices(i,2), indices_params(i,1))
            case ('TR')
              call TR(indices(i,2), indices_params(i,1))
            case ('TX10p')
              call TX10p(indices(i,2), indices_params(i,1))  
            case ('TN10p')
              call TN10p(indices(i,2), indices_params(i,1))
            case ('TX90p')
              call TX90p(indices(i,1), indices(i,2), indices_params(i,1))
            case ('TN90p')
              call TN90p(indices(i,1), indices(i,2), indices_params(i,1))
            case ('WSDI')
              call WSDI_TX90p(indices(i,2), indices_params(i,1), int(indices_params(i,2)))  
            case ('CSDI')
              call CSDI_TN10p(indices(i,2), indices_params(i,1), int(indices_params(i,2)))
            case ('Rnnmm')
              call Rnnmm(name_Rnnmm, indices(i,2), int(indices_params(i,1)))
            case ('R95p')
              call R95p(indices(i,2), indices_params(i,1), int(indices_params(i,2)))
            case ('PRCPTOT')
              call PRCPTOT(indices(i,2), indices_params(i,1))
            case ('CDD')
              call CDD(indices(i,2), int(indices_params(i,1)))
            case ('CWD')
              call CWD(indices(i,2), int(indices_params(i,1)))
            case ('Rx5day')
              call Rx5day(indices(i,2), int(indices_params(i,1)))
            case ('GSL')
              call GSL(indices(i,2), indices_params(i,1), int(indices_params(i,2)), &
                       indices_params(i,3), int(indices_params(i,4)))
          end select
        enddo
      endif

      stnnum=stnnum+1

    enddo   ! end loop over all stations

    stnnum=stnnum-1
    write(ID_log,*) "Total ",stnnum," stations were calculated !"

    call get_time(1,ID_log,tto)  ! get final running time
    close(ID_log)

    print*,'Total time usage is about ',real(tto-tfrom),' seconds..'

    stop
    end program Fclimdex
