! ----------------------------------------------------------------------
! Calculate index Rx1day, Rx5day, and also output (last) time for Rx1day.
!
! definitions are:
! Rx1day, Monthly maximum 1-day precipitation.
! Rx5day, Monthly maximum consecutive 5-day precipitation.
! date: (last) time for the extreme 1-day prcp. format is MMDD
!
! input
!    none
!   (transfer data through the module COMM)
!
      subroutine RX5day
      use COMM
      use functions
      implicit none
      integer :: year, month, day,cnt,k,kth,i, Ky ! loop index and/or count #.
      real    :: r1(YRS,13), r5(YRS,13), r5prcp   ! index to be calculated
      integer :: date(YRS,13)                     ! date for the extreme PRCP

      if(Prcp_miss) return 

      cnt=0
      r1=MISSING
      r5=MISSING
      date=0
      do i=1,YRS
        year=i+SYEAR-1
        Ky=leapyear(year)+1
        do month=1,12
          Kth=Mon(month,Ky)
          do day=1,kth
            cnt=cnt+1
      ! get Rx5day
            if(cnt.ge.5)then   ! corrected by Imke, original was 'cnt .gt. 5'  -- 2012.7.9
              r5prcp=0.
              do k=cnt-4,cnt
                if(nomiss(PRCP(k)))then
                  r5prcp=r5prcp+PRCP(k)
                endif
              enddo
            else
              r5prcp=MISSING
            endif
            if(nomiss(PRCP(cnt)).and.(ismiss(r1(i,month))	&   ! Rx1day
!              .or.PRCP(cnt).gt.r1(i,month))) then
              .or.PRCP(cnt).ge.r1(i,month))) then   ! H.Yang to get the last date
              r1(i,month)=PRCP(cnt)
              date(i,month)=month*100+day
            endif
            if(nomiss(PRCP(cnt)).and.r5prcp.gt.r5(i,month)) then  ! Rx5day
              r5(i,month)=r5prcp
            endif
          enddo  ! loop for day
          if(MNASTAT(i,month,1).eq.1) then  ! If monthly PRCP is MISSING, set monthly index=MISSING 
            r1(i,month)=MISSING
            r5(i,month)=MISSING
            date(i,month)=0
          endif
          if(nomiss(r1(i,month)).and.(ismiss(r1(i,13))	  &  ! annual Rx1day
            .or.r1(i,month).gt.r1(i,13))) then
            r1(i,13)=r1(i,month)
            date(i,13)=month*100+day
          endif
          if(nomiss(r5(i,month)).and.(ismiss(r5(i,13))	  &  ! annual Rx5day
            .or.r5(i,month).gt.r5(i,13))) then
            r5(i,13)=r5(i,month)
          endif
        enddo     ! loop for month
        
! check if annual PRCP is MISSING
! if yes, set annual index=MISSING
        if(YNASTAT(i,1).eq.1) then
          r1(i,13)=MISSING
          r5(i,13)=MISSING
          date(i,13)=0
        endif
      enddo     ! loop for year

! output the index

      call save_monthly(15,r1)

      call save_monthly(16,r5)

      call save_time(45,date)

      return
      end subroutine RX5day
